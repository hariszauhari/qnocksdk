package id.codigo.qnocklib;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import id.codigo.qnocklib.model.TokenGenerate;
import id.codigo.qnocklib.services.TokenServices;
import id.codigo.seedroid.service.ServiceListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String token = FirebaseInstanceId.getInstance().getToken();

        Log.i("Token", token);

        TokenServices tokenServices= new TokenServices();
        tokenServices.tokenGenerate(new ServiceListener <TokenGenerate> () {
            @Override
            public void onSuccess(TokenGenerate tokenGenerate) {
                Log.i("String", tokenGenerate.toString());
            }

            @Override
            public void onFailed(String s) {
                Log.i("Test", s);
            }
        }, "e3zD1hvVJ0Q:APA91bERfjQCAowF_1kAI7tFW-Ttp0LIcLXAFyEU69TE-mMSBdnamEPZpmhF7Kf6mUPvTbP7CwUoeAnU2B4ND7uMfHwQ0NgWq5HiUuVUnCYBZxK6qEC0Lkbvh7lOvzI431EMglcVjYoB", "Berita olimpiyade 2016", "Berita olimpiyade 2016", "Berita olimpiyade 2016", "bx824BJqw3", "#ghjmSDV", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImNvZGlnbyIsImV4cCI6MTQ3MzI0MDkwMDkzOSwiaWF0IjoxNDczMTU0NTAwfQ.lHMZ747nOUQdx1j_m-Kz8v2hOamoLlriEmMZk_ECM4U");

        FirebaseMessaging.getInstance().subscribeToTopic("suram");
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d("intent extra", "Key: " + key + " Value: " + value);
            }
        }
    }
}
