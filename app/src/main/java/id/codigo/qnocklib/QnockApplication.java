package id.codigo.qnocklib;

import id.codigo.seedroid.SeedroidApplication;
import id.codigo.seedroid.configs.RestConfigs;

/**
 * Created by Codigo on 9/28/2017.
 */

public class QnockApplication extends SeedroidApplication {
    public QnockApplication(){
        RestConfigs.isUsingBasicAuth = true;
        RestConfigs.basicAuthValue = "Basic YWRtaW46MTIzNA==";

        RestConfigs.rootUrl = "http://push.Qnock.netconnect.stg.codigo.id";
    }
}
