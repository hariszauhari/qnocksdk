package id.codigo.qnocklib.interfaces;

import java.util.ArrayList;

import id.codigo.qnocklib.model.SubsChannel;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface SubscribeInterface {
    void onSubscribeGet(boolean status, String msg, String response);
}
