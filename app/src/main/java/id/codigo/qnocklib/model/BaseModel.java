package id.codigo.qnocklib.model;

/**
 * Created by Codigo on 9/28/2017.
 */

public class BaseModel {
    String status;
    String time;

    public String getSTATUS() {
        return status;
    }

    public void setSTATUS(String status) {
        this.status = status;
    }

    public String getTIME() {
        return time;
    }

    public void setTIME(String time) {
        this.time = time;
    }

}
