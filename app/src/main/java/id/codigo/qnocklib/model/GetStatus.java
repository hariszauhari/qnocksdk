package id.codigo.qnocklib.model;

/**
 * Created by Codigo on 9/28/2017.
 */

public class GetStatus extends BaseModel {
    private String data;

    public String getDATA() {
        return data;
    }

    public void setDATA(String data) {
        this.data = data;
    }

}
