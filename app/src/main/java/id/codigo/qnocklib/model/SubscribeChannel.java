package id.codigo.qnocklib.model;

import java.util.ArrayList;

/**
 * Created by Codigo on 9/28/2017.
 */

public class SubscribeChannel extends BaseModel {
    private ArrayList<SubsChannel> data;

    public ArrayList<SubsChannel> getDATA() {
        return data;
    }

    public void setDATA(ArrayList<SubsChannel> data) {
        this.data = data;
    }
}