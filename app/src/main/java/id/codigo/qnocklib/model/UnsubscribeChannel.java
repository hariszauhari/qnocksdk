package id.codigo.qnocklib.model;

import java.util.ArrayList;

/**
 * Created by Codigo on 9/28/2017.
 */

public class UnsubscribeChannel extends BaseModel {
    private ArrayList<UnSubsChannel> data;

    public ArrayList<UnSubsChannel> getDATA() {
        return data;
    }

    public void setDATA(ArrayList<UnSubsChannel> data) {
        this.data = data;
    }
}

