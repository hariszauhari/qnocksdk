package id.codigo.qnocklib.services;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.model.CallbackAndroid;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 9/29/2017.
 */

public class CallbackServices {
    public void androidCallback (ServiceListener <CallbackAndroid> callback, String secret, String token1, String id){
        String url = RestConfigs.rootUrl + "/callback/android";

        HttpHelper.getInstance().get(url, callback);

        Map<String, String> impression = new HashMap<>();
        impression.put("app_secret", secret);
        impression.put("Token", token1);
        impression.put("Unix_id", id);

        HttpHelper.getInstance().post(url, impression, callback);

    }
}