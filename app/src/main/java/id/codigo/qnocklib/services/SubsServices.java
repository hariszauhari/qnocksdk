package id.codigo.qnocklib.services;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.model.SubscribeChannel;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 9/29/2017.
 */

public class SubsServices {
    public void subscribeUser (ServiceListener <SubscribeChannel> callback, String tokenId, String channel, String device, String token){
        String url = RestConfigs.rootUrl + "/user/subscribe";

        HttpHelper.getInstance().get(url, callback);

        Map<String, String> subscribe = new HashMap<>();
        subscribe.put("user_token_id", tokenId);
        subscribe.put("channel", channel);
        subscribe.put("device", device);
        subscribe.put("token", token);
        HttpHelper.getInstance().post(url, subscribe, callback);
    }
    public void subscribeUser (ServiceListener <SubscribeChannel> callback, JSONObject params){
        String url = RestConfigs.rootUrl + "/user/subscribe";
        HttpHelper.getInstance().post(url, null, params, callback);
    }
}
