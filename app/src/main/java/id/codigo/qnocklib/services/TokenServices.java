package id.codigo.qnocklib.services;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.model.TokenGenerate;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 9/28/2017.
 */

public class TokenServices {
    public void tokenGenerate(ServiceListener <TokenGenerate> callback, String tokenId,
                              String message, String alert, String body, String secret, String id, String tokenCode){
        String url = RestConfigs.rootUrl + "/token";

        HttpHelper.getInstance().get(url, callback);

        Map<String, String> token = new HashMap<>();
        token.put("user_token_id", tokenId);
        token.put("message", message);
        token.put("alert", alert);
        token.put("body", body);
        token.put("app_secret", secret);
        token.put("unixId", id);
        token.put("Token", tokenCode);

        HttpHelper.getInstance().post(url, token, callback);
    }



}
